#include "VertexBufferObject.h"

VertexArrayObject::VertexArrayObject()
{
}

VertexArrayObject::~VertexArrayObject()
{
}

int VertexArrayObject::addVBO(VertexBufferData descriptor)
{
	return 0;
}

VertexBufferData * VertexArrayObject::getVboData(AttributeLocations loc)
{
	return nullptr;
}

GLuint VertexArrayObject::getVaoHandle() const
{
	return GLuint();
}

GLenum VertexArrayObject::getPrimitiveType() const
{
	return GLenum();
}

GLuint VertexArrayObject::getVboHandle(AttributeLocations loc) const
{
	return GLuint();
}

void VertexArrayObject::createVAO(GLenum vboUsage)
{
}

void VertexArrayObject::reuploadVAO()
{
}

void VertexArrayObject::draw() const
{
}

void VertexArrayObject::bind() const
{
}

void VertexArrayObject::unbind() const
{
}

void VertexArrayObject::destroy()
{
}
